import { defineStore } from 'pinia'
import Axios from 'axios'

export const useDefaultStore = defineStore({
  id: 'default',
  state: () => ({
   /**
    *  tournois:
    [
      {
        "id": 1,
        "nom": "Ligue des champions de l'UEFA",
        "date_debut": "2022-09-13T00:00:00+00:00",
        "date_fin": "2023-05-27T00:00:00+00:00",
        "equipes": [2, 3, 4, 5, 6, 7, 8, 10, 11, 12, 13, 15, 16, 17, 18, 19],
        "games": [1, 2, 3, 4, 5, 6, 7, 8],
        "dateDebut": "2022-09-13T00:00:00+00:00",
        "dateFin": "2023-05-27T00:00:00+00:00"
      },
      {
        "id": 2,
        "nom": "Europa League",
        "date_debut": "2022-09-15T00:00:00+00:00",
        "date_fin": "2023-05-24T00:00:00+00:00",
        "equipes": [1, 2, 3, 4, 5, 6, 7, 8, 10, 11, 13, 14, 15, 16, 17, 19],
        "games": [9, 10, 11, 12, 13, 14, 15, 16],
        "dateDebut": "2022-09-15T00:00:00+00:00",
        "dateFin": "2023-05-24T00:00:00+00:00"
      },
      {
        "id": 3,
        "nom": "Premier League",
        "date_debut": "2022-08-13T00:00:00+00:00",
        "date_fin": "2023-05-21T00:00:00+00:00",
        "equipes": [1, 2, 3, 5, 7, 8, 9, 10, 11, 12, 13, 15, 16, 17, 18, 19],
        "games": [17, 18, 19, 20, 21, 22, 23, 24],
        "dateDebut": "2022-08-13T00:00:00+00:00",
        "dateFin": "2023-05-21T00:00:00+00:00"
      },
      {
        "id": 4,
        "nom": "Liga",
        "date_debut": "2022-08-12T00:00:00+00:00",
        "date_fin": "2023-05-28T00:00:00+00:00",
        "equipes": [1, 2, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 16, 17, 18],
        "games": [25, 26, 27, 28, 29, 30, 31, 32],
        "dateDebut": "2022-08-12T00:00:00+00:00",
        "dateFin": "2023-05-28T00:00:00+00:00"
      },
      {
        "id": 5,
        "nom": "Serie A",
        "date_debut": "2022-08-21T00:00:00+00:00",
        "date_fin": "2023-05-28T00:00:00+00:00",
        "equipes": [1, 2, 3, 4, 5, 6, 7, 8, 11, 12, 13, 14, 15, 16, 18, 19],
        "games": [33, 34, 35, 36, 37, 38, 39, 40],
        "dateDebut": "2022-08-21T00:00:00+00:00",
        "dateFin": "2023-05-28T00:00:00+00:00"
      },
      {
        "id": 6,
        "nom": "Bundesliga",
        "date_debut": "2022-08-13T00:00:00+00:00",
        "date_fin": "2023-05-20T00:00:00+00:00",
        "equipes": [1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 12, 13, 16, 17, 18, 19],
        "games": [41, 42, 43, 44, 45, 46, 47, 48],
        "dateDebut": "2022-08-13T00:00:00+00:00",
        "dateFin": "2023-05-20T00:00:00+00:00"
      }
    ]
    ,
        joueurs: [
      {
        "id": 1,
        "nom": "Messi",
        "prenom": "Lionel",
        "nationalite": "Argentine",
        "equipe": "4",
        "dateNaissance": "1987-06-24T00:00:00+00:00"
      },
      {
        "id": 2,
        "nom": "Ronaldo",
        "prenom": "Cristiano",
        "nationalite": "Portugal",
        "equipe": "12",
        "dateNaissance": "1985-02-05T00:00:00+00:00"
      },
      {
        "id": 3,
        "nom": "Da Silva",
        "prenom": "Neymar",
        "nationalite": "Brésil",
        "equipe": "16",
        "dateNaissance": "1992-02-05T00:00:00+00:00"
      },
      {
        "id": 4,
        "nom": "Mbappé",
        "prenom": "Kylian",
        "nationalite": "France",
        "equipe": "17",
        "dateNaissance": "1998-12-20T00:00:00+00:00"
      },
      {
        "id": 5,
        "nom": "De Bruyne",
        "prenom": "Kevin",
        "nationalite": "Belgique",
        "equipe": "9",
        "dateNaissance": "1991-06-28T00:00:00+00:00"
      },
      {
        "id": 6,
        "nom": "Salah",
        "prenom": "Mohamed",
        "nationalite": "Egypte",
        "equipe": "10",
        "dateNaissance": "1992-06-15T00:00:00+00:00"
      },
      {
        "id": 7,
        "nom": "Lewandowski",
        "prenom": "Robert",
        "nationalite": "Pologne",
        "equipe": "13",
        "dateNaissance": "1988-08-21T00:00:00+00:00"
      },
      {
        "id": 8,
        "nom": "Kane",
        "prenom": "Harry",
        "nationalite": "Angleterre",
        "equipe": "17",
        "dateNaissance": "1993-07-28T00:00:00+00:00"
      },
      {
        "id": 9,
        "nom": "Mané",
        "prenom": "Sadio",
        "nationalite": "Sénégal",
        "equipe": "8",
        "dateNaissance": "1992-04-10T00:00:00+00:00"
      },
      {
        "id": 10,
        "nom": "van Dijk",
        "prenom": "Virgil",
        "nationalite": "Pays-Bas",
        "equipe": "3",
        "dateNaissance": "1991-07-08T00:00:00+00:00"
      }
    
    ],
    equipes: [
  {
    "id": 1,
    "nom": "Real Madrid",
    "imageName": "rma.svg",
    "tournoi": [2, 3, 4, 5, 6],
    "joueurs": [],
    "games": [21, 39]
  },
  {
    "id": 2,
    "nom": "FC Barcelone",
    "imageName": "fcb.webp",
    "tournoi": [1, 2, 3, 4, 5, 6],
    "joueurs": [],
    "games": [1, 17, 35]
  },
  {
    "id": 3,
    "nom": "Liverpool FC",
    "imageName": "liverpool.webp",
    "tournoi": [1, 2, 3, 4, 5, 6],
    "joueurs": [10],
    "games": [11, 32, 36, 47]
  },
  {
    "id": 4,
    "nom": "Manchester City",
    "imageName": "manc.png",
    "tournoi": [1, 2, 4, 5, 6],
    "joueurs": [1],
    "games": [5, 9, 31, 41]
  },
  {
    "id": 5,
    "nom": "Bayern Munich",
    "imageName": "bayern.png",
    "tournoi": [1, 2, 3, 5, 6],
    "joueurs": [],
    "games": [13, 24, 45]
  },
  {
    "id": 6,
    "nom": "Paris Saint-Germain",
    "imageName": "psg.png",
    "tournoi": [1, 2, 4, 5, 6],
    "joueurs": [],
    "games": [7, 27]
  },
  {
    "id": 7,
    "nom": "Juventus FC",
    "imageName": "juve.png",
    "tournoi": [1, 2, 3, 4, 5, 6],
    "joueurs": [],
    "games": [2, 16, 34]
  },
  {
    "id": 8,
    "nom": "AC Milan",
    "imageName": "milan.webp",
    "tournoi": [1, 2, 3, 4, 5, 6],
    "joueurs": [9],
    "games": [4, 10, 19, 28, 48]
  },
  {
    "id": 9,
    "nom": "Chelsea FC",
    "imageName": "chelsea.webp",
    "tournoi": [3, 4, 6],
    "joueurs": [5],
    "games": []
  },
  {
    "id": 10,
    "nom": "Arsenal FC",
    "imageName": "arsenal.png",
    "tournoi": [1, 2, 3, 4],
    "joueurs": [6],
    "games": [3, 22]
  },
  {
    "id": 11,
    "nom": "Manchester United",
    "imageName": "manu.png",
    "tournoi": [1, 2, 3, 4, 5, 6],
    "joueurs": [],
    "games": [18, 40, 46]
  },
  {
    "id": 12,
    "nom": "Tottenham Hotspur",
    "imageName": "spurs.webp",
    "tournoi": [1, 3, 4, 5, 6],
    "joueurs": [2],
    "games": [6, 20, 25, 38]
  },
  {
    "id": 13,
    "nom": "Borussia Dortmund",
    "imageName": "bvb.webp",
    "tournoi": [1, 2, 3, 4, 5, 6],
    "joueurs": [7],
    "games": [12, 30]
  },
  {
    "id": 14,
    "nom": "Inter Milan",
    "imageName": "inter.png",
    "tournoi": [2, 4, 5],
    "joueurs": [],
    "games": [14]
  },
  {
    "id": 15,
    "nom": "AS Roma",
    "imageName": "roma.png",
    "tournoi": [1, 2, 3, 5],
    "joueurs": [],
    "games": [23]
  },
  {
    "id": 16,
    "nom": "Ajax Amsterdam",
    "imageName": "ajax.svg",
    "tournoi": [1, 2, 3, 4, 5, 6],
    "joueurs": [3],
    "games": [8, 26, 37]
  },
  {
    "id": 17,
    "nom": "FC Porto",
    "imageName": "porto.png",
    "tournoi": [1, 2, 3, 4, 6],
    "joueurs": [4, 8],
    "games": [15, 29, 43]
  },
  {
    "id": 18,
    "nom": "Olympique Lyonnais",
    "imageName": "OL.png",
    "tournoi": [1, 3, 4, 5, 6],
    "joueurs": [],
    "games": [44]
  },
  {
    "id": 19,
    "nom": "CODM",
    "imageName": "codm.png",
    "tournoi": [1, 2, 3, 5, 6],
    "joueurs": [],
    "games": [33, 42]
  }
]
,
    games:
    [
  {
    "id": 1,
    "date": "2023-05-22T00:00:00+00:00",
    "equipeDomicile": 2,
    "equipeVisiteur": 15,
    "score_domicile": 4,
    "score_visiteur": 4,
    "tournoi": 1,
    "scoreDomicile": 4,
    "scoreVisiteur": 4
  },
  {
    "id": 2,
    "date": "2023-05-22T00:00:00+00:00",
    "equipeDomicile": 7,
    "equipeVisiteur": 13,
    "score_domicile": 1,
    "score_visiteur": 4,
    "tournoi": 1,
    "scoreDomicile": 1,
    "scoreVisiteur": 4
  },
  {
    "id": 3,
    "date": "2023-05-22T00:00:00+00:00",
    "equipeDomicile": 10,
    "equipeVisiteur": 17,
    "score_domicile": 3,
    "score_visiteur": 2,
    "tournoi": 1,
    "scoreDomicile": 3,
    "scoreVisiteur": 2
  },
  {
    "id": 4,
    "date": "2023-05-22T00:00:00+00:00",
    "equipeDomicile": 8,
    "equipeVisiteur": 5,
    "score_domicile": 2,
    "score_visiteur": 1,
    "tournoi": 1,
    "scoreDomicile": 2,
    "scoreVisiteur": 1
  },
  {
    "id": 5,
    "date": "2023-05-22T00:00:00+00:00",
    "equipeDomicile": 4,
    "equipeVisiteur": 3,
    "score_domicile": 4,
    "score_visiteur": 4,
    "tournoi": 1,
    "scoreDomicile": 4,
    "scoreVisiteur": 4
  },
  {
    "id": 6,
    "date": "2023-05-22T00:00:00+00:00",
    "equipeDomicile": 12,
    "equipeVisiteur": 15,
    "score_domicile": 1,
    "score_visiteur": 3,
    "tournoi": 1,
    "scoreDomicile": 1,
    "scoreVisiteur": 3
  },
  {
    "id": 7,
    "date": "2023-05-22T00:00:00+00:00",
    "equipeDomicile": 6,
    "equipeVisiteur": 18,
    "score_domicile": 4,
    "score_visiteur": 3,
    "tournoi": 1,
    "scoreDomicile": 4,
    "scoreVisiteur": 3
  },
  {
    "id": 8,
    "date": "2023-05-22T00:00:00+00:00",
    "equipeDomicile": 16,
    "equipeVisiteur": 11,
    "score_domicile": 3,
    "score_visiteur": 1,
    "tournoi": 1,
    "scoreDomicile": 3,
    "scoreVisiteur": 1
  },
  {
    "id": 9,
    "date": "2023-05-22T00:00:00+00:00",
    "equipeDomicile": 4,
    "equipeVisiteur": 15,
    "score_domicile": 2,
    "score_visiteur": 1,
    "tournoi": 2,
    "scoreDomicile": 2,
    "scoreVisiteur": 1
  },
  {
    "id": 10,
    "date": "2023-05-22T00:00:00+00:00",
    "equipeDomicile": 8,
    "equipeVisiteur": 11,
    "score_domicile": 2,
    "score_visiteur": 2,
    "tournoi": 2,
    "scoreDomicile": 2,
    "scoreVisiteur": 2
  },
  {
    "id": 11,
    "date": "2023-05-22T00:00:00+00:00",
    "equipeDomicile": 3,
    "equipeVisiteur": 16,
    "score_domicile": 3,
    "score_visiteur": 2,
    "tournoi": 2,
    "scoreDomicile": 3,
    "scoreVisiteur": 2
  },
  {
    "id": 12,
    "date": "2023-05-22T00:00:00+00:00",
    "equipeDomicile": 13,
    "equipeVisiteur": 15,
    "score_domicile": 1,
    "score_visiteur": 4,
    "tournoi": 2,
    "scoreDomicile": 1,
    "scoreVisiteur": 4
  },
  {
    "id": 13,
    "date": "2023-05-22T00:00:00+00:00",
    "equipeDomicile": 5,
    "equipeVisiteur": 6,
    "score_domicile": 4,
    "score_visiteur": 3,
    "tournoi": 2,
    "scoreDomicile": 4,
    "scoreVisiteur": 3
  },
  {
    "id": 14,
    "date": "2023-05-22T00:00:00+00:00",
    "equipeDomicile": 14,
    "equipeVisiteur": 1,
    "score_domicile": 4,
    "score_visiteur": 1,
    "tournoi": 2,
    "scoreDomicile": 4,
    "scoreVisiteur": 1
  },
  {
    "id": 15,
    "date": "2023-05-22T00:00:00+00:00",
    "equipeDomicile": 17,
    "equipeVisiteur": 10,
    "score_domicile": 4,
    "score_visiteur": 1,
    "tournoi": 2,
    "scoreDomicile": 4,
    "scoreVisiteur": 1
  },
  {
    "id": 16,
    "date": "2023-05-22T00:00:00+00:00",
    "equipeDomicile": 7,
    "equipeVisiteur": 2,
    "score_domicile": 1,
    "score_visiteur": 4,
    "tournoi": 2,
    "scoreDomicile": 1,
    "scoreVisiteur": 4
  },
  {
    "id": 17,
    "date": "2023-05-22T00:00:00+00:00",
    "equipeDomicile": 2,
    "equipeVisiteur": 15,
    "score_domicile": 3,
    "score_visiteur": 2,
    "tournoi": 3,
    "scoreDomicile": 3,
    "scoreVisiteur": 2
  },
  {
    "id": 18,
    "date": "2023-05-22T00:00:00+00:00",
    "equipeDomicile": 11,
    "equipeVisiteur": 3,
    "score_domicile": 4,
    "score_visiteur": 2,
    "tournoi": 3,
    "scoreDomicile": 4,
    "scoreVisiteur": 2
  },
  {
    "id": 19,
    "date": "2023-05-22T00:00:00+00:00",
    "equipeDomicile": 8,
    "equipeVisiteur": 9,
    "score_domicile": 2,
    "score_visiteur": 1,
    "tournoi": 3,
    "scoreDomicile": 2,
    "scoreVisiteur": 1
  },
  {
    "id": 20,
    "date": "2023-05-22T00:00:00+00:00",
    "equipeDomicile": 12,
    "equipeVisiteur": 16,
    "score_domicile": 4,
    "score_visiteur": 1,
    "tournoi": 3,
    "scoreDomicile": 4,
    "scoreVisiteur": 1
  },
  {
    "id": 21,
    "date": "2023-05-22T00:00:00+00:00",
    "equipeDomicile": 1,
    "equipeVisiteur": 18,
    "score_domicile": 1,
    "score_visiteur": 2,
    "tournoi": 3,
    "scoreDomicile": 1,
    "scoreVisiteur": 2
  },
  {
    "id": 22,
    "date": "2023-05-22T00:00:00+00:00",
    "equipeDomicile": 10,
    "equipeVisiteur": 13,
    "score_domicile": 1,
    "score_visiteur": 3,
    "tournoi": 3,
    "scoreDomicile": 1,
    "scoreVisiteur": 3
  },
  {
    "id": 23,
    "date": "2023-05-22T00:00:00+00:00",
    "equipeDomicile": 15,
    "equipeVisiteur": 7,
    "score_domicile": 1,
    "score_visiteur": 2,
    "tournoi": 3,
    "scoreDomicile": 1,
    "scoreVisiteur": 2
  },
  {
    "id": 24,
    "date": "2023-05-22T00:00:00+00:00",
    "equipeDomicile": 5,
    "equipeVisiteur": 17,
    "score_domicile": 1,
    "score_visiteur": 1,
    "tournoi": 3,
    "scoreDomicile": 1,
    "scoreVisiteur": 1
  },
  {
    "id": 25,
    "date": "2023-05-22T00:00:00+00:00",
    "equipeDomicile": 12,
    "equipeVisiteur": 14,
    "score_domicile": 4,
    "score_visiteur": 4,
    "tournoi": 4,
    "scoreDomicile": 4,
    "scoreVisiteur": 4
  },
  {
    "id": 26,
    "date": "2023-05-22T00:00:00+00:00",
    "equipeDomicile": 16,
    "equipeVisiteur": 11,
    "score_domicile": 3,
    "score_visiteur": 3,
    "tournoi": 4,
    "scoreDomicile": 3,
    "scoreVisiteur": 3
  },
  {
    "id": 27,
    "date": "2023-05-22T00:00:00+00:00",
    "equipeDomicile": 6,
    "equipeVisiteur": 9,
    "score_domicile": 1,
    "score_visiteur": 1,
    "tournoi": 4,
    "scoreDomicile": 1,
    "scoreVisiteur": 1
  },
  {
    "id": 28,
    "date": "2023-05-22T00:00:00+00:00",
    "equipeDomicile": 8,
    "equipeVisiteur": 7,
    "score_domicile": 1,
    "score_visiteur": 3,
    "tournoi": 4,
    "scoreDomicile": 1,
    "scoreVisiteur": 3
  },
  {
    "id": 29,
    "date": "2023-05-22T00:00:00+00:00",
    "equipeDomicile": 17,
    "equipeVisiteur": 10,
    "score_domicile": 3,
    "score_visiteur": 4,
    "tournoi": 4,
    "scoreDomicile": 3,
    "scoreVisiteur": 4
  },
  {
    "id": 30,
    "date": "2023-05-22T00:00:00+00:00",
    "equipeDomicile": 13,
    "equipeVisiteur": 2,
    "score_domicile": 2,
    "score_visiteur": 2,
    "tournoi": 4,
    "scoreDomicile": 2,
    "scoreVisiteur": 2
  }
],
    */
   
    
    


  }),
  getters: {
    /**
     * nombreTournois() {
      return this.tournois.length;
    },
    nbEquipes() {
      return this.equipes.length;
    },
    nbMatchs() {
      return this.games.length;
    },
    nbJoueurs() {
      return this.joueurs.length;
    },
     */
  },
  actions: {
   /**
    *  loadData() {
      
    }
    */
  }
})
