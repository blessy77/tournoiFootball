import { createRouter, createWebHashHistory } from 'vue-router'
import IndexVue from '../components/Index.vue'
import TournoiVue from '../components/Tournoi.vue'
import EquipeVue from '../components/Equipe.vue'
import JoueurVue from '../components/Joueur.vue'
import GameVue from '../components/Game.vue'
import EquipeViewVue from '../components/EquipeView.vue'
import GameViewVue from '../components/GameView.vue'

const routes = [
    // À compléter
    {
      path: '/',
      name : 'index',
      component : IndexVue
  },
  {
      path: '/tournoi',
      name : 'tournoi',
      component : TournoiVue
  },
  {
      path: '/equipe',
      name : 'equipes',
      component : EquipeVue
  },
  {
      path: '/joueur',
      name : 'joueur',
      component : JoueurVue
  },
   {
      path: '/game',
      name : 'games',
      component : GameVue
    },
    {
      path: '/equipe/:nom',
      name : 'equipe',
      component : EquipeViewVue
    },
    {
      path: '/game/:id',
      name : 'game',
      component : GameViewVue
  }
  
]

const router = createRouter({
    history: createWebHashHistory(),
    routes,
  })

export default router