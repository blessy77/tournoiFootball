<?php

namespace App\Entity;

use App\Repository\GameRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Metadata\ApiResource;

#[ORM\Entity(repositoryClass: GameRepository::class)]
#[ApiResource]
class Game
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: Types::DATE_MUTABLE)]
    private ?\DateTimeInterface $date = null;

    #[ORM\ManyToOne(targetEntity: Equipe::class, inversedBy: 'games')]
    #[ORM\JoinColumn(name: 'equipe_domicile_id', referencedColumnName: 'id', onDelete: 'CASCADE')]
    private ?Equipe $equipeDomicile = null;

    #[ORM\ManyToOne(targetEntity: Equipe::class, inversedBy: 'games')]
    #[ORM\JoinColumn(name: 'equipe_visiteur_id', referencedColumnName: 'id', onDelete: 'CASCADE')]
    private ?Equipe $equipeVisiteur = null;

    #[ORM\Column]
    private ?int $score_domicile = null;

    #[ORM\Column]
    private ?int $score_visiteur = null;

    #[ORM\ManyToOne(inversedBy: 'games', cascade: ['remove'])]
    private ?Tournoi $tournoi = null;




    /**
     * @param \DateTimeInterface|null $date
     * @param int|null $score_domicile
     * @param int|null $score_visiteur
     */
    public function __construct(?\DateTimeInterface $date = new \DateTime(), ?int $score_domicile = null, ?int $score_visiteur = null)
    {
        $this->date = $date;
        $this->score_domicile = $score_domicile?? rand(1,4);
        $this->score_visiteur = $score_visiteur ?? rand(1,4);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getEquipeDomicile(): ?Equipe
    {
        return $this->equipeDomicile;
    }

    public function setEquipeDomicile(?Equipe $equipeDomicile): self
    {
        $this->equipeDomicile = $equipeDomicile;

        return $this;
    }

    public function getEquipeVisiteur(): ?Equipe
    {
        return $this->equipeVisiteur;
    }

    public function setEquipeVisiteur(?Equipe $equipeVisiteur): self
    {
        $this->equipeVisiteur = $equipeVisiteur;

        return $this;
    }

    public function getScoreDomicile(): ?int
    {
        return $this->score_domicile;
    }

    public function setScoreDomicile(int $score_domicile): self
    {
        $this->score_domicile = $score_domicile;

        return $this;
    }

    public function getScoreVisiteur(): ?int
    {
        return $this->score_visiteur;
    }

    public function setScoreVisiteur(int $score_visiteur): self
    {
        $this->score_visiteur = $score_visiteur;

        return $this;
    }

    public function getTournoi(): ?Tournoi
    {
        return $this->tournoi;
    }

    public function setTournoi(?Tournoi $tournoi): self
    {
        $this->tournoi = $tournoi;

        return $this;
    }



}
