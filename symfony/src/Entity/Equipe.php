<?php

namespace App\Entity;

use App\Repository\EquipeRepository;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use ApiPlatform\Metadata\ApiResource;

#[ORM\Entity(repositoryClass: EquipeRepository::class)]
#[Vich\Uploadable]
#[ApiResource]
class Equipe
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id ;
    #[ORM\Column(type: 'datetime', nullable: true)]
    private ?\DateTimeInterface $updatedAt = null;

    #[ORM\Column(length: 255)]
    private ?string $nom ;

    // NOTE: This is not a mapped field of entity metadata, just a simple property.
    #[Vich\UploadableField(mapping: 'team_images', fileNameProperty: 'imageName')]
    private ?File $imageFile = null;

    #[ORM\Column(nullable: true)]
    private ?string $imageName = null;




    #[ORM\ManyToMany(targetEntity: Tournoi::class, inversedBy: 'equipes')]
    private Collection $tournoi;


    #[ORM\OneToMany(mappedBy: 'equipe', targetEntity: Joueur::class, cascade: ["remove"])]
    private Collection $joueurs;

    #[ORM\OneToMany(mappedBy: 'equipeDomicile', targetEntity: Game::class, cascade: ["remove"])]
    private Collection $games;






    public function __construct($nom = '', $imageName='')
    {
        $this->nom = $nom;
        $this->imageName = $imageName;

        $this->tournoi = new ArrayCollection();
        $this->games = new ArrayCollection();
        $this->joueurs = new ArrayCollection();
    }
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }




    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile|null $imageFile
     */
    public function setImageFile(?File $imageFile = null): void
    {
        $this->imageFile = $imageFile;

        if (null !== $imageFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTimeImmutable();
        }
    }

    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    public function setImageName(?string $imageName): void
    {
        $this->imageName = $imageName;
    }

    public function getImageName(): ?string
    {
        return $this->imageName;
    }






    /**
     * @return Collection<int, Tournoi>
     */
    public function getTournoi(): Collection
    {
        return $this->tournoi;
    }

    public function addTournoi(Tournoi $tournoi): self
    {
        if (!$this->tournoi->contains($tournoi)) {
            $this->tournoi->add($tournoi);
        }

        return $this;
    }

    public function removeTournoi(Tournoi $tournoi): self
    {
        $this->tournoi->removeElement($tournoi);

        return $this;
    }
    /**
     * @return Collection<int, Joueur>
     */
    public function getJoueurs(): Collection
    {
        return $this->joueurs;
    }

    public function addJoueur(Joueur $joueur): self
    {
        if (!$this->joueurs->contains($joueur)) {
            $this->joueurs->add($joueur);
            $joueur->setEquipe($this);
        }

        return $this;
    }

    public function removeJoueur(Joueur $joueur): self
    {
        if ($this->joueurs->removeElement($joueur)) {
            // set the owning side to null (unless already changed)
            if ($joueur->getEquipe() === $this) {
                $joueur->setEquipe(null);
            }
        }

        return $this;
    }

    public function __toString(): string
    {
        // TODO: Implement __toString() method.
        return $this->nom;
    }

    /**
     * @return Collection<int, Game>
     */
    public function getgames(): Collection
    {
        return $this->games;
    }

    public function addgames(Game $games): self
    {
        if (!$this->games->contains($games)) {
            $this->games->add($games);
            $games->setEquipeDomicile($this);
        }

        return $this;
    }

    public function removegames(Game $games): self
    {
        if ($this->games->removeElement($games)) {
            // set the owning side to null (unless already changed)
            if ($games->getEquipeDomicile() === $this) {
                $games->setEquipeDomicile(null);
            }
        }

        return $this;
    }

    public function addGame(Game $game): self
    {
        if (!$this->games->contains($game)) {
            $this->games->add($game);
            $game->setEquipeVisiteur($this);
        }

        return $this;
    }

    public function removeGame(Game $game): self
    {
        if ($this->games->removeElement($game)) {
            // set the owning side to null (unless already changed)
            if ($game->getEquipeVisiteur() === $this) {
                $game->setEquipeVisiteur(null);
            }
        }

        return $this;
    }


}
