<?php

namespace App\Controller;

use App\Entity\Game;
use App\Form\GameType;
use App\Repository\GameRepository;
use App\Repository\JoueurRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/game')]
class GameController extends AbstractController
{
    private $joueurRepository;

    public function __construct(JoueurRepository $joueurRepository)
    {
        $this->joueurRepository = $joueurRepository;
    }
    #[Route('/', name: 'app_game_index', methods: ['GET'])]
    public function index(GameRepository $gameRepository): Response
    {
        return $this->render('game/index.html.twig', [
            'games' => $gameRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_game_new', methods: ['GET', 'POST'])]
    public function new(Request $request, GameRepository $gameRepository): Response
    {
        $game = new Game();
        $form = $this->createForm(GameType::class, $game);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $gameRepository->save($game, true);

            return $this->redirectToRoute('app_game_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('game/new.html.twig', [
            'game' => $game,
            'form' => $form,
        ]);
    }
    // fonction privee pour recuperer des joueurs aleatoires pour les equipes qui ont pas de joueurs
    private function getRandomJoueurs(int $count = 11): array
    {
        $joueurs = [];
        // Obtenir des joueurs aléatoires de l'entité "Joueur"
        $allJoueurs = $this->joueurRepository->findAll();
        if (count($allJoueurs) <= $count) {
            $joueurs = $allJoueurs;
        } else {
            $randomKeys = array_rand($allJoueurs, $count);
            foreach ($randomKeys as $key) {
                $joueurs[] = $allJoueurs[$key];
            }
        }

        return $joueurs;
    }



    #[Route('/{id}', name: 'app_game_show', methods: ['GET'])]
    public function show(Game $game): Response
    {
        $buteursDomicile = [];
        $buteursVisiteur = [];

        $scoreDomicile = $game->getScoreDomicile();
        $scoreVisiteur = $game->getScoreVisiteur();

        $equipeDomicile = $game->getEquipeDomicile();
        $equipeVisiteur = $game->getEquipeVisiteur();

        $joueursDomicile = $equipeDomicile->getJoueurs()->toArray();
        $joueursVisiteur = $equipeVisiteur->getJoueurs()->toArray();

        if (empty($joueursDomicile)) {
            // Ajouter des joueurs aléatoires à l'équipe domicile
            $joueursDomicile = $this->getRandomJoueurs();
            foreach ($joueursDomicile as $joueur) {
                $equipeDomicile->addJoueur($joueur);
            }
        }
        if (empty($joueursVisiteur)) {
            // Ajouter des joueurs aléatoires à l'équipe visiteur
            $joueursVisiteur = $this->getRandomJoueurs();
            foreach ($joueursVisiteur as $joueur) {
                $equipeVisiteur->addJoueur($joueur);
            }
        }
        if ($scoreDomicile > 0 && $scoreVisiteur > 0) {
            for ($i = 0; $i < $scoreDomicile; $i++) {
                $buteurDomicile = $joueursDomicile[array_rand($joueursDomicile)];
                while (in_array($buteurDomicile, $buteursVisiteur)) {
                    $buteurDomicile = $joueursDomicile[array_rand($joueursDomicile)];
                }
                $buteursDomicile[] = $buteurDomicile;
            }

            for ($i = 0; $i < $scoreVisiteur; $i++) {
                $buteurVisiteur = $joueursVisiteur[array_rand($joueursVisiteur)];
                while (in_array($buteurVisiteur, $buteursDomicile)) {
                    $buteurVisiteur = $joueursVisiteur[array_rand($joueursVisiteur)];
                }
                $buteursVisiteur[] = $buteurVisiteur;
            }
        } else if ($scoreDomicile > 0) {
            for ($i = 0; $i < $scoreDomicile; $i++) {
                $buteurDomicile = $joueursDomicile[array_rand($joueursDomicile)];
                $buteursDomicile[] = $buteurDomicile;
            }
        } else if ($scoreVisiteur > 0) {
            for ($i = 0; $i < $scoreVisiteur; $i++) {
                $buteurVisiteur = $joueursVisiteur[array_rand($joueursVisiteur)];
                $buteursVisiteur[] = $buteurVisiteur;
            }
        }

        return $this->render('game/show.html.twig', [
            'game' => $game,
            'buteursDomicile' => $buteursDomicile,
            'buteursVisiteur' => $buteursVisiteur,
        ]);
    }




    #[Route('/{id}/edit', name: 'app_game_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Game $game, GameRepository $gameRepository): Response
    {
        $form = $this->createForm(GameType::class, $game);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $gameRepository->save($game, true);

            return $this->redirectToRoute('app_game_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('game/edit.html.twig', [
            'game' => $game,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_game_delete', methods: ['POST'])]
    public function delete(Request $request, Game $game, GameRepository $gameRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$game->getId(), $request->request->get('_token'))) {
            $gameRepository->remove($game, true);
        }

        return $this->redirectToRoute('app_game_index', [], Response::HTTP_SEE_OTHER);
    }
}
