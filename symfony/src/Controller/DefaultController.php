<?php

namespace App\Controller;

use App\Repository\EquipeRepository;
use App\Repository\GameRepository;
use App\Repository\JoueurRepository;
use App\Repository\TournoiRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    #[Route('/', name: 'app_home')]
    public function index(EquipeRepository $equipeRepository, TournoiRepository $tournoiRepository, JoueurRepository $joueurRepository, GameRepository $gameRepository): Response
    {
        $nbTournois = $tournoiRepository->count([]);
        $nbJoueurs = $joueurRepository->count([]);
        $nbMatchs = $gameRepository->count([]);
        $nbEquipes = $equipeRepository->count([]);
        return $this->render('default/index.html.twig', [
            'nbTournois' => $nbTournois,
            'nbJoueurs' => $nbJoueurs,
            'nbMatchs' => $nbMatchs,
            'nbEquipes' => $nbTournois,
        ]);
    }
}
