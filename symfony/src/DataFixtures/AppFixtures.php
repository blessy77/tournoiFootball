<?php

namespace App\DataFixtures;

use App\Entity\Equipe;
use App\Entity\Game;
use App\Entity\Joueur;
use App\Entity\Tournoi;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    private $passwordHasher;

    public function __construct(UserPasswordHasherInterface $passwordHasher)
    {
        $this->passwordHasher = $passwordHasher;
    }
    public function load(ObjectManager $manager): void
    {
        $users = [
            [
                'email' => 'john@example.com',
                'password' => 'password1',
                'roles' => ['ROLE_USER'],
            ],
            [
                'email' => 'jane@example.com',
                'password' => 'password2',
                'roles' => ['ROLE_ADMIN'],
            ],
        ];
        $joueurs = [
            [
                "prenom" => "Lionel",
                "nom" => "Messi",
                "nationalite" => "Argentine",
                "date_naissance" => new \DateTime("1987-06-24")
            ],
            [
                "prenom" => "Cristiano",
                "nom" => "Ronaldo",
                "nationalite" => "Portugal",
                "date_naissance" => new \DateTime("1985-02-05")
            ],
            [
                "prenom" => "Neymar",
                "nom" => "Jr",
                "nationalite" => "Brésil",
                "date_naissance" => new \DateTime("1992-02-05")
            ],
            [
                "prenom" => "Kylian",
                "nom" => "Mbappé",
                "nationalite" => "France",
                "date_naissance" => new \DateTime("1998-12-20")
            ],
            [
                "prenom" => "Kevin",
                "nom" => "De Bruyne",
                "nationalite" => "Belgique",
                "date_naissance" => new \DateTime("1991-06-28")
            ],
            [
                "prenom" => "Mohamed",
                "nom" => "Salah",
                "nationalite" => "Egypte",
                "date_naissance" => new \DateTime("1992-06-15")
            ],
            [
                "prenom" => "Robert",
                "nom" => "Lewandowski",
                "nationalite" => "Pologne",
                "date_naissance" => new \DateTime("1988-08-21")
            ],
            [
                "prenom" => "Harry",
                "nom" => "Kane",
                "nationalite" => "Angleterre",
                "date_naissance" => new \DateTime("1993-07-28")
            ],
            [
                "prenom" => "Sadio",
                "nom" => "Mané",
                "nationalite" => "Sénégal",
                "date_naissance" => new \DateTime("1992-04-10")
            ],
            [
                "prenom" => "Virgil",
                "nom" => "van Dijk",
                "nationalite" => "Pays-Bas",
                "date_naissance" => new \DateTime("1991-07-08")
            ]
        ];
        $equipes = [
            [
                "nom" => "Real Madrid",
                "logo" => "rma.svg"
            ],
            [
                "nom" => "FC Barcelone",
                "logo" => "fcb.webp"
            ],
            [
                "nom" => "Liverpool FC",
                "logo" => "liverpool.webp"
            ],
            [
                "nom" => "Manchester City",
                "logo" => "manc.png"
            ],
            [
                "nom" => "Bayern Munich",
                "logo" => "bayern.png"
            ],
            [
                "nom" => "Paris Saint-Germain",
                "logo" => "psg.png"
            ],
            [
                "nom" => "Juventus FC",
                "logo" => "juve.png"
            ],
            [
                "nom" => "AC Milan",
                "logo" => "milan.webp"
            ],
            [
                "nom" => "Chelsea FC",
                "logo" => "chelsea.webp"
            ],
            [
                "nom" => "Arsenal FC",
                "logo" => "arsenal.png"
            ],
            [
                "nom" => "Manchester United",
                "logo" => "manu.png"
            ],
            [
                "nom" => "Tottenham Hotspur",
                "logo" => "spurs.webp"
            ],
            [
                "nom" => "Borussia Dortmund",
                "logo" => "bvb.webp"
            ],
            [
                "nom" => "Inter Milan",
                "logo" => "inter.png"
            ],
            [
                "nom" => "AS Roma",
                "logo" => "roma.png"
            ],
            [
                "nom" => "Ajax Amsterdam",
                "logo" => "ajax.svg"
            ],
            
            [
                "nom" => "FC Porto",
                "logo" => "porto.png"
            ],
            [
                "nom" => "Olympique Lyonnais",
                "logo" => "OL.png"
            ],

            [
                "nom" => "CODM",
                "logo" => "codm.png"
            ]
        ];

        $tournois = [
            [
                "nom" => "Ligue des champions de l'UEFA",
                "date_debut" => new \DateTime("2022-09-13"),
                "date_fin" => new \DateTime("2023-05-27")
            ],
            [
                "nom" => "Europa League",
                "date_debut" => new \DateTime("2022-09-15"),
                "date_fin" => new \DateTime("2023-05-24")
            ],
            [
                "nom" => "Premier League",
                "date_debut" => new \DateTime("2022-08-13"),
                "date_fin" => new \DateTime("2023-05-21")
            ],
            [
                "nom" => "Liga",
                "date_debut" => new \DateTime("2022-08-12"),
                "date_fin" => new \DateTime("2023-05-28")
            ],
            [
                "nom" => "Serie A",
                "date_debut" => new \DateTime("2022-08-21"),
                "date_fin" => new \DateTime("2023-05-28")
            ],
            [
                "nom" => "Bundesliga",
                "date_debut" => new \DateTime("2022-08-13"),
                "date_fin" => new \DateTime("2023-05-20")
            ]
        ];

        $teams = [];
        $tournaments = [];

        foreach ($equipes as $e) {
            $equipe = new Equipe($e['nom'], $e['logo']);
            $teams[] = $equipe;
            $manager->persist($equipe);
        }

        foreach ($joueurs as $j) {
            $joueur = new Joueur($j['nom'], $j['prenom'], $j['date_naissance'], $j['nationalite']);
            $equipeAleatoire = $teams[array_rand($teams)];
            $joueur->setEquipe($equipeAleatoire);
            $manager->persist($joueur);
        }

        foreach ($tournois as $t) {
            $tournoi = new Tournoi($t['nom'], $t['date_debut'], $t['date_fin']);

            $equipesAleatoires = $teams;
            shuffle($equipesAleatoires);

            for ($i = 0; $i < 16; $i++) {
                $equipeAleatoire = array_shift($equipesAleatoires);
                $tournoi->addEquipe($equipeAleatoire);
            }

            $tournaments[] = $tournoi;
            $manager->persist($tournoi);
        }

        $matches = [];

        foreach ($tournaments as $tournoi) {
            $equipesAleatoires = $tournoi->getEquipes()->toArray();
            shuffle($equipesAleatoires);

            while (count($equipesAleatoires) > 1) {
                $equipe1 = array_shift($equipesAleatoires);
                $equipe2 = array_shift($equipesAleatoires);

                $match = new Game();
                $match->setTournoi($tournoi);
                $match->setEquipeDomicile($equipe1);
                $match->setEquipeVisiteur($equipe2);
                $matches[] = $match;
                $manager->persist($match);
            }
        }
        foreach ($users as $userData) {
            $user = new User();
            $user->setEmail($userData['email']);
            $user->setRoles($userData['roles']);

            $hashedPassword = $this->passwordHasher->hashPassword($user, $userData['password']);
            $user->setPassword($hashedPassword);

            $manager->persist($user);
        }

        $manager->flush();
    }
}
