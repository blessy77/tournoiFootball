


var dropdown = document.querySelector(".dropdown");


var dropdownMenu = document.querySelector(".dropdown-menu");


dropdown.addEventListener("click", function() {

    if (dropdownMenu.style.display === "block") {

        dropdownMenu.style.display = "none";
    } else {

        dropdownMenu.style.display = "block";
    }
});

dropdown.addEventListener("mouseout", function() {
    if (dropdownMenu.style.display !== "block") {
        dropdownMenu.style.display = "none";
    }
});