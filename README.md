# Tournoi de Football

Ce projet est une application pour gérer des tournois de football. Il utilise le framework Symfony pour le backend et Vue.js pour le frontend.

## Entités

### Tournoi
Représente un tournoi de football

Attributs :
- id : identifiant unique du tournoi
- nom : nom du tournoi
- date_debut : date de début du tournoi
- date_fin : date de fin du tournoi

Relations :
- Un tournoi peut avoir plusieurs équipes (OneToMany)

### Equipe
Représente une équipe de football

Attributs :
- id : identifiant unique de l'équipe
- nom : nom de l'équipe
- logo : logo de l'équipe

Relations :
- Une équipe peut participer à plusieurs tournois (ManyToMany)
- Une équipe peut avoir plusieurs joueurs (OneToMany)

### Joueur
Représente un joueur de football

Attributs :
- id : identifiant unique du joueur
- nom : nom du joueur
- prénom : prénom du joueur
- date_naissance : date de naissance du joueur
- nationalité : nationalité du joueur

Relations :
- Un joueur appartient à une équipe (ManyToOne)

### Match
Représente un match entre deux équipes

Attributs :
- id : identifiant unique du match
- date : date du match
- score_equipe1 : score de l'équipe 1
- score_equipe2 : score de l'équipe 2

Relations :
- Un match a lieu entre deux équipes (ManyToOne)
- Un match fait partie d'un tournoi (ManyToOne)

## Composants Vue.js

### Header

Ce composant affiche l'en-tête et le footer de l'application.

### Tournoi

Ce composant affiche la liste des tournois existants.

### Equipe

Ce composant affiche la liste des équipes existantes.

### EquipeView

Ce composant affiche les détails d'une équipe spécifique.

### Joueur

Ce composant affiche la liste des joueurs existants.

### Game

Ce composant affiche la liste des matchs existants.

### GameView

Ce composant affiche les détails d'un match spécifique.

## Instructions de développement

1. Clonez ce projet sur votre machine locale.
2. Dans le dossier `appVue`, exécutez les commandes suivantes pour installer les dépendances et lancer l'application Vue.js :
   
   ```bash
   npm install
   npm run dev
    ```
1. Ouvrez un nouveau terminal.
2. Dans le dossier `symfony`, configurez l'URL de la base de données dans le fichier .env en remplaçant DATABASE_URL par les informations appropriées :
   ```shell
   DATABASE_URL=mysql://user:password@host:port/database_name
   ```
   Remplacez user, password, host, port et database_name par les valeurs spécifiques à votre configuration de base de données.

3. Exécutez les commandes suivantes pour configurer le backend Symfony et lancer le serveur :



    ```bash
    composer require symfony/runtime
    php bin/console doctrine:database:create
    php bin/console doctrine:migrations:migrate
    php bin/console doctrine:fixtures:load
    symfony server:start
    ```
